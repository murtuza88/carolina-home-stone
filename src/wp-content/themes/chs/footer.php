<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->
	<footer id="colophon" role="contentinfo">
		<div class="site-info">
			<div class="footer-content">
				<div class="footer-content1">
				<div class="footer-title">
						Navigation
					</div>
					<ul class="char-footer footer1-text" style="float:left;padding-right:15px;">
					
						<!--<li>
							<a href="#">Site Map</a>
						</li>
			--><?php
			global $wpdb;
			$child_pages = $wpdb->get_results("SELECT id,post_title FROM $wpdb->posts WHERE post_parent = 14    AND post_type = 'page' limit 0,6", 'OBJECT');    
			?>
			<?php if ( $child_pages ) : foreach ( $child_pages as $pageChild ) : ?>
						<li>
							<a href="<?php echo  get_permalink($pageChild->id); ?>"  title="<?php echo $pageChild->post_title; ?>"><?php echo $pageChild->post_title; ?></a>
						</li>
			<?php endforeach; endif; ?>
					</ul>
					<ul class="char-footer footer1-text">
						
						<?php
			global $wpdb;
			$child_pages = $wpdb->get_results("SELECT id,post_title FROM $wpdb->posts WHERE post_parent = 14    AND post_type = 'page' limit 6,6", 'OBJECT');    
			?>
			<?php if ( $child_pages ) : foreach ( $child_pages as $pageChild ) : ?>
						<li>
							<a href="<?php echo  get_permalink($pageChild->id); ?>"  title="<?php echo $pageChild->post_title; ?>"><?php echo $pageChild->post_title; ?></a>
						</li>
			<?php endforeach; endif; ?>
					</ul>
				</div>
				<div class="footer-content2">
				<div class="footer-title">
						Stay Connected
					</div>
					<ul class="char-footer">
					<li>
						<a target="_blank" href="https://www.facebook.com/Carolinas.stone"><img style="margin-right: 5px" src="<?php echo get_template_directory_uri(); ?>/images/facebook.png"></a>
						<a target="_blank" href="https://plus.google.com/110742559388838513245/posts"><img style="margin-left:5px;margin-right: 3px;margin-bottom:4px;" width="40px" src="<?php echo get_template_directory_uri(); ?>/images/google_plus.png"></a>
						<img style="margin-left:5px;margin-right: 3px" src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"><br/>
						<img width="44px" style="margin-right: 5px" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
						<a target="_blank" href="http://www.bbb.org/charlotte/business-reviews/granite/carolinas-stone-marble-and-granite-in-monroe-nc-126154"><img height="42px" style="margin-right: 5px" src="<?php echo get_template_directory_uri(); ?>/images/IconBBB.png"></a>
					</li>
				</ul>
				<br/>
				<div class="footer-title"><!--Countertop Installation--></div>
				<!--  <img src="<?php echo get_template_directory_uri(); ?>/images/footer-image.jpg"></img>--> 
				</div>
				<div class="footer-content3">
					<div class="footer-title">
						Serving Charlotte
					</div>
					<ul class="char-footer footer3-text">
						<p class="footer">Serving Charlotte, North Carolina with custom granite countertops and commercial surface fabrication.
						<br /><br />
					 Albermarle, Ballantyne, Belmont, Birkdale Village
					 Chester, Concord, Denver, Dilworth
					 Downtown, Forrest City, Ft. Mill, Gaffney
				 	 Gastonia, Harrisburg, Hickory, Huntersville
					 Iron Station, Kannapolis, Kings Mountain, Lake Norman
					 Lake Wylie, Lancaster, Lenoir, Lexington
					 Maiden, Lincolnton, Locust, Mint Hill
					 Mocksville, Monroe, Morganton, Morrisville
					 Myers Park, Newton, NoDa, North Wilksboro
					 Pineville, Plaza-Midwood, Rock Hill, Salisbury
					 Shelby, South End, South Park, Stanley
					 Statesville, Troutman, University, Winston Salem
						
						</p>
					</ul>
				</div>
			</div><!-- .footer-content -->
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>