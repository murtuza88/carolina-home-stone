<?php
/*
 * Template Name: Home
 * */
?>
<?php 
	get_header();
?>
<div>
	<div class="home-left-bar">
		<aside id="left-text-1" class="widget_text widget">
			<h3 class="widget-title">Facebook Activity</h3>
			<div class="textwidget left-text-1-content">
				<iframe scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:210px; height:350px" allowtransparency="true" src="http://www.facebook.com/plugins/fan.php?id=Carolinas.stone&amp;width=210&amp;connections=5&amp;stream=true&amp;header=false&amp;locale=en_US"></iframe>
			</div>
		</aside>
		<aside id="left-text-2" class="widget_text widget">
			<h3 class="widget-title">How it Works</h3>
			<div>			
				<div class="video-title">From the Quarry to the Kitchen</div>
				<div class="left-textwidget">
					<a href="<?php echo SITE_NAME?>/?page_id=61#quarry">
						<img src="../wp-content/themes/chs/images/quarry.png">
					</a>
				</div>
			</div>
			<div>			
				<div class="video-title">Care & Cleaning of Natural Stone</div>
				<div class="left-textwidget">
					<a href="<?php echo SITE_NAME?>/?page_id=61#care_cleanning">
						<img src="../wp-content/themes/chs/images/care_cleanning.png">
					</a>
				</div>
			</div>
			<div>			
				<div class="video-title">The Story of Natural Stone</div>
				<div class="left-textwidget">
				<a href="<?php echo SITE_NAME?>/?page_id=61#story">
					<img src="../wp-content/themes/chs/images/story_natural_stone.png">
				</a>
				</div>
			</div>
			<div>			
				<div class="video-title">The History of Man and Stone: A Documentary</div>
				<div class="left-textwidget">
				<a href="<?php echo SITE_NAME?>/?page_id=61#history">
					<img src="../wp-content/themes/chs/images/history_stone.png">
				</a>
				</div>
			</div>
		</aside>
		</div>
	<div class="home-content">
		<?php 
	    $your_query = new WP_Query( 'pagename=home' );
	    while ( $your_query->have_posts() ) : $your_query->the_post();
    	?>
    <div class="entry-content">
    	<?php the_content();?>
    	<?php 
 		if ( has_post_thumbnail()){?>
	    	<div class="promotional-image">
	    		<?php the_post_thumbnail( 'large');?>
	    	</div>
    	<?php }?>	
    </div>
        <?php endwhile; ?>
	</div>
	<?php get_sidebar();?>
</div>

<?php
get_footer();
?>