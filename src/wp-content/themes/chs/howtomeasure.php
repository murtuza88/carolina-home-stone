<?php
/*Template Name: Measure*/
get_header();

if($_POST) {
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
	$to = 'nickmehtausa@gmail.com';
	$message = '
<html>
<head>
  <title>Calculation from website</title>
</head>
<body>
  <table>
    <tr>
      <td>Countertop Sq. Ft.:</td><td>'.$_POST['total_countertop'].'</td>
    </tr>
    <tr>
      <td>Backsplash Sq. Ft.:</td><td>'.$_POST['total_backsplash'].'</td>
    </tr>
    <tr>
      <td>Total Sq. Ft.:</td><td>'.$_POST['Total_Sq_Ft'].'</td>
    </tr>
    <tr>
      <td>Material:</td><td>'.$_POST['material'].'</td>
    </tr>
    <tr>
      <td>Color:</td><td>'.$_POST['color'].'</td>
    </tr>
    <tr>
      <td>Edge:</td><td>'.$_POST['edge'].'</td>
    </tr>
    <tr>
      <td>Sinktype:</td><td>'.$_POST['sinktype'].'</td>
    </tr>
    <tr>
      <td>Date Needed:</td><td>'.$_POST['dateneeded'].'</td>
    </tr>
    <tr>
      <td>Name:</td><td>'.$_POST['yourname'].'</td>
    </tr>
    <tr>
      <td>Phone:</td><td>'.$_POST['yourphone'].'</td>
    </tr>
    <tr>
      <td>Email:</td><td>'.$_POST['youremail'].'</td>
    </tr>
  </table>
</body>
</html>
';
	$response = mail($to, 'User Calculated Countertop', $message,$headers);
}
?>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.add-more-counter').click(function(){
			var newtr = jQuery('.counter-template').clone();
			var counter = String.fromCharCode(jQuery('.counter:last').html().charCodeAt() + 1);
			jQuery(newtr).find('td:eq(0) label').attr('class','counter').html(counter);
			
			jQuery('<tr>'+newtr.html()+'</tr>').insertBefore('.counter-last');
			});

	jQuery('.add-more-back').click(function(){
		var newtr = jQuery('.back-template').clone();
		var counter = String.fromCharCode(jQuery('.back:last').html().charCodeAt() + 1);
		jQuery(newtr).find('td:eq(0) label').attr('class','back').html(counter);
		
		jQuery('<tr>'+newtr.html()+'</tr>').insertBefore('.back-last');
		});

	
		});
	function calcForm() {
		var countertopsqft = 0.0;
		jQuery('.calculate-counter-length').each(function(index){
			var length = jQuery(this).val();
			var width = jQuery('.calculate-counter-width')[index].value;
			var total = ((parseInt(isNaN(length) || length == '' ? '0' : length) * parseInt(isNaN(width) || width == '' ? '0' : width)) / 144).toFixed(2);
			countertopsqft = parseFloat(countertopsqft) + parseFloat(total);
			jQuery('.calculate-counter-total')[index].value = total;
		});
		jQuery('#Total_countertop').val(parseFloat(countertopsqft).toFixed(2));
}

	function calcForm_back() {
		var backtopsqft = 0.0;
		jQuery('.calculate-back-length').each(function(index){
			var length_back = jQuery(this).val();
			var width_back = jQuery('.calculate-back-width')[index].value;
			var total_back = ((parseInt(isNaN(length_back) || length_back == '' ? '0' : length_back) * parseInt(isNaN(width_back) || width_back == '' ? '0' : width_back)) / 144).toFixed(2);
			backtopsqft = parseFloat(backtopsqft) + parseFloat(total_back);
			jQuery('.calculate-back-total')[index].value = total_back;
		});
		jQuery('#Total_backsplash').val(parseFloat(backtopsqft).toFixed(2));
}
	function calctotal() {
		var totalcounter = jQuery('#Total_countertop').val();
		var totalback = jQuery('#Total_backsplash').val();
		var totalsqft = (parseFloat(isNaN(totalcounter) || totalcounter == '' ? '0' : totalcounter) + parseFloat(isNaN(totalback) || totalback == '' ? '0' : totalback)).toFixed(2);
		jQuery('#Total_Sq_Ft').val(totalsqft);
		
		}
</script>
<div>
	<div style="float:left;width:70%" >
		<header class="entry-header">
			<h1 class="entry-title">How to Measure</h1>
		</header>

		<div class="how-to-messure">
			<p>
				Measuring your countertops has never been easier. Just fill in the information in the form below to describe your kitchen. Or click here for a pdf version of this form that you can print out, fill out, and fax to us at 704-896-0017.	
				Click the layout below that resembles your kitchen to see how to break down the sections for measuring.
			</p>
			<p>
				<b>STEP 1:</b><br/>
				Click the layout below that resembles your kitchen to see how to break down the sections for measuring.
			</p>
			<?php add_thickbox(); ?>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/Rectangle_Boxed.jpg" class="thickbox" ><img src="<?php echo get_template_directory_uri(); ?>/images/Rectangle.jpg" width="100" height="31" alt="rectangle" /></a>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/LShape_Boxed.jpg" target="_blank" class="thickbox" id="click_l"><img src="<?php echo get_template_directory_uri(); ?>/images/LShape.jpg" width="100" height="47" alt="L" /></a>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/UShape_Boxed.jpg" target="_blank" class="thickbox" id="click_u"><img src="<?php echo get_template_directory_uri(); ?>/images/UShape.jpg" width="100" height="39" alt="U" /></a>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/Single45_Boxed.jpg" target="_blank" class="thickbox" id="click_lcorner"><img src="<?php echo get_template_directory_uri(); ?>/images/Single45.jpg" width="100" height="56" alt="single_45" /></a>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/Double45_Boxed.jpg" target="_blank" class="thickbox" id="click_angledl"><img src="<?php echo get_template_directory_uri(); ?>/images/Double45.jpg" width="100" height="56" alt="double_45" /></a><br>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/BatWing_Boxed.jpg" target="_blank" class="thickbox" id="click_bat"><img src="<?php echo get_template_directory_uri(); ?>/images/BatWing.jpg" width="100" height="44" alt="bat_wing" /></a>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/Unique_Boxed.jpg" target="_blank" class="thickbox" id="click_shoe"><img src="<?php echo get_template_directory_uri(); ?>/images/Unique.jpg" width="100" height="41" alt="unique" /></a>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/BumpOut_Rectangle.jpg" target="_blank" class="thickbox" id="click_bump"><img src="<?php echo get_template_directory_uri(); ?>/images/BumpOut.jpg" width="100" height="34" alt="bump_out" /></a>
		    <a href="<?php echo get_template_directory_uri(); ?>/images/LongRadius_Boxed.jpg" target="_blank" class="thickbox" id="click_bowed"><img src="<?php echo get_template_directory_uri(); ?>/images/LongRadius.jpg" width="100" height="30" alt="radius" /></a></h2>
			<p></p>
			<p>
				<b>STEP 2:</b><br/>
				Enter your measurement information below in our online estimator to calculate the total square footage. Enter the length and width in inches for each section, then click the Calculate button. The calculator will calculate the section's square footage for you. There are three different Calculate buttons -- be sure to click all three.
			</p>
			<form method="post">
			<div id="how-to-measure-table">
				<table cellspacing="0" cellpadding="2" border="0" width="400" class="smaller">
		         <tbody><tr>
		                <td><strong>Countertops</strong></td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		              </tr>
		              <tr>
		                <td>&nbsp;</td>
		                <td><div align="center">Length(in.) </div></td>
		                <td><div align="center">Width(in.)</div></td>
		                <td>&nbsp;</td>
		                <td><div align="center">Section Sq. Ft.</div></td>
		              </tr>
		              <tr>
		                <td>Section <label>A</label>:</td>
		                <td><input type="text" id="countertoplengthA"  class="calculate-counter-length"></td>
		                <td><input type="text" id="countertopwidthA"  class="calculate-counter-width"></td>
		                <td>=</td>
		                <td><input type="text" id="countertoptotalA"  class="calculate-counter-total"></td>
		              </tr>
		              <tr>
		                <td>Section <label class="counter">B</label>: </td>
		                <td><input type="text" id="countertoplengthB" class="calculate-counter-length"></td>
		                <td><input type="text" id="countertopwidthB" class="calculate-counter-width"></td>
		                <td>=</td>
		                <td><input type="text" id="countertoptotalB"  class="calculate-counter-total"></td>
		              </tr>
		              <tr style="display:none;" class="counter-template">
		                <td>Section <label></label>: </td>
		                <td><input type="text" id="countertoplengthC"  class="calculate-counter-length"></td>
		                <td><input type="text" id="countertopwidthC"  class="calculate-counter-width"></td>
		                <td>=</td>
		                <td><input type="text" id="countertoptotalC"  class="calculate-counter-total"></td>
		              </tr>
		              <tr class="counter-last">
		                <td style="padding:6px 20px;"><span class="add-more-counter add-link">+ Add More Section</span></td>
		                <td>&nbsp;</td>
		                <td ></td>
		                <td></td>
		                <td></td>
		              </tr>
		              <tr>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td valign="top"><div align="right">Countertop Sq. Ft.</div></td>
		                <td>=</td>
		                <td><input type="text" id="Total_countertop" name="total_countertop">
		                <br>
		                <input type="button" value="Calculate" onclick="calcForm()" id="button"></td>
		              </tr>
		              <tr>
		                <td><strong>Backsplash</strong></td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		              </tr>
		              <tr>
		                <td>&nbsp;</td>
		                <td><div align="center">Length(in.) &nbsp;</div></td>
		                <td><div align="center">Height(in.)	&nbsp;</div></td>
		                <td>&nbsp;</td>
		                <td><div align="center">Section Sq. Ft.</div></td>
		              </tr>
		              <tr>
		                <td>Section <label class="back">A</label>:</td>
		                <td><input type="text" id="backsplashlengthA" class="calculate-back-length"></td>
		                <td><input type="text" id="backsplashheightA" class="calculate-back-width"></td>
		                <td>=</td>
		                <td><input type="text" id="backsplashtotalA" class="calculate-back-total"></td>
		              </tr>
		              <tr>
		                <td>Section <label class="back">B</label>: </td>
		                <td><input type="text" id="backsplashlengthB" class="calculate-back-length"></td>
		                <td><input type="text" id="backsplashheightB" class="calculate-back-width"></td>
		                <td>=</td>
		                <td><input type="text" id="backsplashtotalB" class="calculate-back-total"></td>
		              </tr>
		              <tr style="display:none;" class="back-template">
		                <td>Section <label></label>: </td>
		                <td><input type="text" id="countertoplengthC" class="calculate-back-length"></td>
		                <td><input type="text" id="countertopwidthC" class="calculate-back-width"></td>
		                <td>=</td>
		                <td><input type="text" id="countertoptotalC" class="calculate-back-total"></td>
		              </tr>
		              <tr class="back-last">
		                <td style="padding:6px 20px;"><span class="add-more-back add-link">+ Add More Section</span></td>
		                <td>&nbsp;</td>
		                <td ></td>
		                <td></td>
		                <td></td>
		              </tr>
		              <tr>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td valign="top"><div align="right">Backsplash Sq. Ft.</div></td>
		                <td>=</td>
		                <td><input type="text" id="Total_backsplash" name="total_backsplash">
		                <br>
		                <input type="button" value="Calculate" onclick="calcForm_back()" id="button2"></td>
		              </tr>
		              <tr>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		              </tr>
		              <tr>
		                <td>&nbsp;</td>
		                <td>&nbsp;</td>
		                <td valign="top"><div align="right"><strong>Total Sq. Ft.</strong></div></td>
		                <td>=</td>
		                <td><input type="text" id="Total_Sq_Ft" name="Total_Sq_Ft">
		                <br>
		                <input type="button" value="Calculate" onclick="calctotal()" id="button3" ></td>
		              </tr>
		            </tbody>
	            </table>
	            <p><strong>* Please note that all calculations are approximate and final sq. ft. for purchase is subject to field measure.</strong></p>
	            <table cellspacing="0" cellpadding="2" border="0" width="100%">
              <tbody><tr>
                <td>&nbsp;</td>
                <td>MATERIAL INFORMATION</td>
              </tr>
              <tr>
                <td>Material</td>
                <td><input type="text" id="material" name="material"></td>
              </tr>
              <tr>
                <td>Color</td>
                <td><input type="text" id="color" name="color"></td>
              </tr>
              <tr>
                <td>Edge (if known)</td>
                <td><input type="text" id="edge" name="edge"></td>
              </tr>
              <tr>
                <td>Sink Type</td>
                <td><select id="sinktype" name="sinktype">
                  <option selected="selected" value="Undermount">Undermount</option>
                  <option value="Drop In">Drop In</option>
                </select></td>
              </tr>
              <tr>
                <td>Date Needed</td>
                <td><input type="text" id="dateneeded" name="dateneeded"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>CONTACT INFORMATION</td>
              </tr>
              <tr>
                <td>Name</td>
                <td><input type="text" id="yourname" name="yourname"></td>
              </tr>
              <tr>
                <td>Phone</td>
                <td><input type="text" id="yourphone" name="yourphone"></td>
              </tr>
              <tr>
                <td>Email</td>
                <td><input type="text" id="youremail" name="youremail"></td>
              </tr>
            </tbody></table>
            <p>
              <input type="submit" value="SUBMIT" id="button4" name="button4">
            </p>
    	      </div>
    	      </form>
		</div>
	</div>
	<?php get_sidebar();?>
</div>
<?php get_footer();?>
