<?php
/* Template Name: Edges */
	get_header();
	$theme_url = get_template_directory_uri();
	$posts = get_posts(array('post_type'=>'edges','order'=> 'ASC', 'orderby' => 'post_excerpt','numberposts'=>-1));
 	$counter = 0;
 	$left_edges_array = array();
 	$right_edges_array = array();
	foreach($posts as $p) {
        $pc = get_post_complete($p->ID);
        if($counter % 2 == 0){
    		$left_edges_array[$counter++] = $pc;
        }else{
    		$right_edges_array[$counter++] = $pc;    	
        }
	}
	$edges_array = array_merge($left_edges_array, $right_edges_array);
	$counter = 0;
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.finishimage:first').show();	
		jQuery('img[id^=change]').click(function(){
			var classname = jQuery(this).attr('id');
			var showimg = classname.split('_');
			jQuery('.finishimage').hide();
			jQuery('#image_'+showimg[1]).show();
			});
		});
 </script>
 <?php $your_query = new WP_Query( 'pagename=edges' );?>
<span class="helptext"><?= get_post_meta($your_query->post->ID,'pagetitle',true ); ?> </span>
<div class="edge-main">
	<div class="edge-left">
		<?php foreach($left_edges_array as $pc) {?>
			<div class="edge-content pull-right">
				<img id="change_<?= $counter++ ?>" class="pull-right" width="138px" height="38px" src="<?= CCTM::filter($pc['edgesample'], 'to_image_src') ?>"/>
				<div class="edges-text-div pull-right">
					<div class="free-text"><?=$pc['post_title'] ?></div>
				</div>
			</div>
		<?php } ?>	
	</div>
	
	<div class="edge-middle edge-middle-image-border">
		<?php foreach($edges_array as $key =>$edges_image) {?>
			<img height="335px" class="hide finishimage" id='image_<?php echo $key?>' src="<?= CCTM::filter($edges_image['edgeimage'], 'to_image_src') ?>"/>
		<?php } ?>	
	</div>
	
	<div class="edge-right">
		<?php foreach($right_edges_array as $pc) {?>
			<div class="edge-content pull-left">
				<img id="change_<?= $counter++ ?>" class="pull-left" width="138px" height="38px" src="<?= CCTM::filter($pc['edgesample'], 'to_image_src') ?>"/>
				<div class="edges-text-div pull-left">
					<div class="free-text"><?= $pc['post_title'] ?></div>
				</div>
			</div>
		<?php } ?>	
	</div>
</div>
<?php 
	while ( $your_query->have_posts() ) : $your_query->the_post();
?>
    <div class="edges-content pull-left">
    	<?php the_content();?>
	</div>
<?php endwhile; ?>
<?php get_footer();?>