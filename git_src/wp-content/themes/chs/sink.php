<?php
/* Template Name: Sink */
	get_header();
	$theme_url = get_template_directory_uri();
	$posts = get_posts(array('post_type'=>'sink','order'=> 'ASC', 'orderby' => 'post_excerpt','numberposts'=>-1));
	
 	$counter = 0;
 	$left_sink_array = array();
 	$right_sink_array = array();
	foreach($posts as $p) {
        $pc = get_post_complete($p->ID);
        if($counter % 2 == 0){
    		$left_sink_array[$counter++] = $pc;
        }else{
    		$right_sink_array[$counter++] = $pc;    	
        }
        
	}
	$sink_array = array_merge($left_sink_array, $right_sink_array);
	$counter = 0;
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.finishimage:first').show();
		
		jQuery('div[id^=change]').click(function(){
			var classname = jQuery(this).attr('id');
			var showimg = classname.split('_');
			jQuery('.finishimage').hide();
				jQuery('#image_'+showimg[1]).show();
			});
		});
</script>
<?php $your_query = new WP_Query( 'pagename=sink' );?>
<span class="helptext"><?= get_post_meta($your_query->post->ID,'pagetitle',true ); ?> </span>
<div class="edge-main">
	<div class="edge-left">
		<?php
			foreach($left_sink_array as $pc) {?>
				<div class="sink-sample-text" style="float:right">
					<div  class="sink-free-text" id="change_<?= $counter++ ?>"><?= $pc['post_title'] ?></div>
				</div>
		<?php } ?>	
	</div>
	
	<div class="edge-middle">
		<?php foreach($sink_array as $key =>$sink_image) {?>
			<img height="384px" id='image_<?php echo $key?>' class="finishimage hide" src="<?= CCTM::filter($sink_image['sinkimage'], 'to_image_src') ?>"/>
		<?php }?>
	</div>
	<div class="edge-right">
		<?php foreach($right_sink_array as $pc) {?>
			<div class="sink-sample-text" style="float:left">
				<div class="sink-free-text" id="change_<?= $counter++ ?>" ><?= $pc['post_title'] ?></div>
			</div>
		<?php } ?>	
	</div>
</div>
	
<?php 
	while ( $your_query->have_posts() ) : $your_query->the_post();
?>
    <div class="sink-content pull-left">
    	<?php the_content();?>
	</div>
	        <?php endwhile; ?>
<?php get_footer();?>