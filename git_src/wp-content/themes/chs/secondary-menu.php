<div id="navigation">
	<div id="access">
		<div class="menu-header">
			<ul class="menu" id="menu-main-nav">
				<li
					class="menu-item menu-item-type-custom menu-item-object-custom menu-item-152"
					id="menu-item-152"><a href="#">Products</a>
				<ul class="sub-menu">
						<?php
						global $wpdb;
			$child_pages = $wpdb->get_results("SELECT id,post_title FROM $wpdb->posts WHERE post_parent = 14    AND post_type = 'page' ", 'OBJECT');    
			?>
			<?php if ( $child_pages ) : foreach ( $child_pages as $pageChild ) : ?>
						<li
							class="menu-item menu-item-type-custom menu-item-object-custom menu-item-837"
							id="menu-item-837">
							<a href="<?php echo  get_permalink($pageChild->id); ?>"  title="<?php echo $pageChild->post_title; ?>"><?php echo $pageChild->post_title; ?></a>
						</li>
			<?php endforeach; endif; ?>
				</ul>
				</li>	
				<li
					class="menu-item menu-item-type-custom menu-item-object-custom menu-item-180"
					id="menu-item-180"><a href="#">Gallery</a>
					<ul class="sub-menu">
						<?php
						global $wpdb;
			$child_pages = $wpdb->get_results("SELECT id,post_title FROM $wpdb->posts WHERE post_parent = 286 AND post_type = 'page' ", 'OBJECT');    
			?>
			<?php if ( $child_pages ) : foreach ( $child_pages as $pageChild ) : ?>
						<li
							class="menu-item menu-item-type-custom menu-item-object-custom menu-item-837"
							id="menu-item-837">
							<a href="<?php echo  get_permalink($pageChild->id); ?>"  title="<?php echo $pageChild->post_title; ?>"><?php echo $pageChild->post_title; ?></a>
						</li>
			<?php endforeach; endif; ?>
				</ul>
					</li>
				<li
					class="menu-item menu-item-type-custom menu-item-object-custom menu-item-180"
					id="menu-item-180"><a href="#">Getting Started</a>
				<ul class="sub-menu">
					<li
						class="menu-item menu-item-type-custom menu-item-object-custom menu-item-179"
						id="menu-item-179"><a
						href="<?php echo SITE_NAME?>/?page_id=101">Sinks</a></li>
					<li
						class="menu-item menu-item-type-custom menu-item-object-custom menu-item-203"
						id="menu-item-203"><a 
						href="<?php echo SITE_NAME?>/?page_id=104">Edges</a></li>
<!--						href="<?php echo SITE_NAME?>/?page_id=104">Edges</a></li>-->
					<li
						class="menu-item menu-item-type-custom menu-item-object-custom menu-item-203"
						id="menu-item-203"><a 
						href="<?php echo SITE_NAME?>/?page_id=317">How to Measure</a></li>
					<li
						class="menu-item menu-item-type-custom menu-item-object-custom menu-item-203"
						id="menu-item-203"><a 
						href="<?php echo SITE_NAME?>/?page_id=324">Copy Ideas</a></li>
				</ul>
				</li>
				<li
					class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"
					id="menu-item-30"><a
					href="<?php echo SITE_NAME?>/?page_id=58">Virtual Kitchen</a>
				</li>
				<li
					class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"
					id="menu-item-29"><a href="<?php echo SITE_NAME?>/?page_id=93">Showroom</a></li>
					<li
					class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"
					id="menu-item-29"><a target="_blank" href="http://www.soulwinning.info/">Devotional</a></li>
					<li
					class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"
					id="menu-item-29"><a href="<?php echo SITE_NAME?>/?page_id=282">Care & Maintenance</a></li>
			</ul>
		</div>
	</div>
<!-- #access -->
</div>
